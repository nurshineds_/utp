import java.util.Scanner;

public class Kasir {
    private Meja[] daftarMeja;
    private Menu[] daftarMenu;

    public Kasir() {
        daftarMeja = new Meja[10];
        for (int i = 0; i < 10; i++) {
            daftarMeja[i] = new Meja(i + 1);
        }

        daftarMenu = new Menu[5];
        daftarMenu[0] = new Menu("Nasi Goreng", 15000);
        daftarMenu[1] = new Menu("Mi Goreng", 15000);
        daftarMenu[2] = new Menu("Capcay", 20000);
        daftarMenu[3] = new Menu("Bihun Goreng", 17000);
        daftarMenu[4] = new Menu("Ayam Koloke", 25000);
    }

    // digunakan untuk menampilkan daftar meja beserta keterangan ketersediaannya
    // gunakan method isKosong pada class Kasir agar lebih mudah
    public void tampilkanDaftarMeja() {
        for (Meja meja : daftarMeja) {
            if (meja.isKosong()) {
                System.out.printf("Meja %d (Kosong)", meja.getNomorMeja());
                // if isKosong is true, the program will display that the meja is kosong
            } else {
                System.out.printf("Meja %d (Terisi oleh pelanggan %s)", meja.getNomorMeja(),
                        meja.getPelanggan().getNama());
                // other wise if isKosong is false, the program will display the client name
            }
        }
    }

    // untuk menambahkan pelanggan pada meja tertentu
    // jika meja kosong tambahkan pelanggan pada meja tersebut
    // jika tidak buatlah keterangan bahwa meja sudah ada pelanggan
    public void tambahPelanggan(int nomorMeja, Pelanggan pelanggan) {
        Meja meja = daftarMeja[nomorMeja - 1];
        if (meja.isKosong()) {
            meja.setPelanggan(pelanggan);
            // if isKosong is true then program will set the pelanggan
        } else {
            System.out.println("Meja telah terisi.");
            // other wise, if isKosong is false then it will display that meja is filled
        }
    }

    // menambah pesanan menu pada nomor meja
    // jika menu tidak ada dalam daftar maka tampilkan "Menu is null"
    public void tambahPesanan(int nomorMeja, Menu menu) {
        // EDIT DISINI
        boolean menuAda = false;
        Meja meja = daftarMeja[nomorMeja - 1];
        for (Menu i : daftarMenu) {
            if (i == menu) {
                menuAda = true;
                // if the menu is available in the list menu, the menuAda will turn to true
                break;
            }
        }
        if (menuAda) {
            meja.setMenu(menu);
            // if menuAda is true, then the program will set the menu
        } else {
            System.out.println("Menu is null.");
            // other wise, the program will display if the menu is null because it's not
            // available in the menu list
        }
    }

    // Menghapus pelanggan
    public void hapusPelanggan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        if (meja.isKosong()) {
            System.out.println("Meja belum terisi.");
            // if the isKosong is true, then the program will display that the meja is not
            // filled by client
        } else {
            meja.setPelanggan(null);
            // other wise, the proram will set the Pelanggan into null
        }
    }

    public int hitungHargaPesanan(int nomorMeja) {
        int totalHarga = 0;
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    totalHarga += menu[i].getHarga();
                }
            }
            return totalHarga;
        }
        return totalHarga;
    }

    public void tampilkanPesanan(int nomorMeja) {
        Meja meja = daftarMeja[nomorMeja - 1];
        Pelanggan pelanggan = meja.getPelanggan();
        Menu[] menu = meja.getMenu();
        if (pelanggan != null && menu != null && menu.length > 0) {
            for (int i = 0; i < menu.length; i++) {
                if (menu[i] != null) {
                    System.out.println("Meja " + nomorMeja + " - " + pelanggan.getNama() + " memesan "
                            + menu[i].getNama() + " seharga " + menu[i].getHarga());
                }
            }
        } else {
            System.out.println("Meja " + nomorMeja + " tidak memiliki pesanan");
        }
    }

    public void tampilkanDaftarMenu() {
        System.out.println("Daftar Menu:");
        System.out.println("1. Nasi Goreng - Rp15.000");
        System.out.println("2. Mi Goreng - Rp15.000");
        System.out.println("3. Capcay - Rp20.000");
        System.out.println("4. Bihun Goreng - Rp17.000");
        System.out.println("5. Ayam Koloke - Rp25.000");
        System.out.println("6. Simpan");
        System.out.println();
    }

    public void tampilkanDaftarFitur() {
        System.out.println("1. Tampilkan daftar meja");
        System.out.println("2. Tambah pelanggan");
        System.out.println("3. Tambah pesanan");
        System.out.println("4. Hapus pelanggan");
        System.out.println("5. Hitung harga pesanan");
        System.out.println("6. Tampilkan pesanan di meja");
        System.out.println("0. Keluar");
    }

    public void jalankan() {
        Scanner scanner = new Scanner(System.in);
        int pilihan = -1;
        while (pilihan != 0) {
            tampilkanDaftarFitur();
            System.out.print("Masukkan pilihan: ");
            pilihan = scanner.nextInt();
            scanner.nextLine();
            switch (pilihan) {
                case 1:
                    // menampilkan daftar meja dengan method yang sudah ada
                    tampilkanDaftarMeja();
                    break;
                case 2:
                    // tampilkan pesan untuk input nomor meja dan nama pelanggan untuk digunakan
                    // pada method
                    // jangan lupa instansiasi Pelanggan dengan nama pelanggan sesuai input
                    // EDIT DISINI
                    System.out.println("Masukkan nomor meja: ");
                    int noMeja = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Masukkan nama pelanggan: ");
                    String namaPelanggan = scanner.nextLine();
                    // calling constructor in Pelanggan class to set the nama pelanggan
                    Pelanggan client = new Pelanggan(namaPelanggan);
                    // calling tambahPelanggan method with parameter noMeja and client as the client
                    // name
                    tambahPelanggan(noMeja, client);
                    break;
                case 3:
                    boolean stopLoop = false;
                    System.out.print("Masukkan nomor meja: ");
                    int nomorMejaPesan = scanner.nextInt();
                    scanner.nextLine();
                    Boolean meja = daftarMeja[nomorMejaPesan - 1].isKosong();
                    if (!meja) {
                        tampilkanDaftarMenu();
                        while (!stopLoop) {
                            System.out.print("Masukkan nomor menu: ");
                            int nomorMenuPesan = scanner.nextInt();
                            scanner.nextLine();
                            switch (nomorMenuPesan) {
                                case 1:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[0]);
                                    break;
                                case 2:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[1]);
                                    break;
                                case 3:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[2]);
                                    break;
                                case 4:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[3]);
                                    break;
                                case 5:
                                    tambahPesanan(nomorMejaPesan, daftarMenu[4]);
                                    break;
                                case 6:
                                    stopLoop = true;
                                    break;
                                default:
                                    System.out.println("Nomor menu tidak valid");
                                    break;
                            }
                        }
                    } else {
                        System.out.println("Meja tidak ada pelanggan");
                    }
                    break;
                case 4:
                    // untuk menghapus pelanggan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja yang akan dihapus untuk digunakan
                    // pada method hapusPelanggan()
                    // EDIT DISINI
                    System.out.println("Masukkan nomor Meja: ");
                    int nomorMejaHapus = scanner.nextInt();
                    // calling hapus pelanggan method, the parameter is filled by nomorMejaHapus
                    hapusPelanggan(nomorMejaHapus);
                    break;
                case 5:
                    // Untuk melihat total harga pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja
                    // jangan lupa membedakan keluaran apabila pelanggan belum memesan apapun /
                    // total harga 0
                    // EDIT DISINI
                    System.out.println("Masukkan nomor meja: ");
                    int nomorMejaTotal = scanner.nextInt();
                    // calling hitungHargaPesanan method to calculating the total price
                    int total = hitungHargaPesanan(nomorMejaTotal);
                    if (total == 0) {
                        System.out.println("Meja tidak memiliki pesanan.");
                        // if the total price is 0 it means the table didnt have any pesanan
                    } else {
                        System.out.printf("Total pesanan pada meja %d adalah %d", nomorMejaTotal, total);
                        // other wise the program will display the total price
                    }
                    break;
                case 6:
                    // untuk melihat pesanan pada meja tertentu
                    // tampilkan pesan untuk memasukkan nomor meja
                    // EDIT DISINI
                    System.out.println("Masukkan nomor meja:");
                    int nomorMejaLihat = scanner.nextInt();
                    // calling tampilkanPesanan method to see the pesanan list
                    tampilkanPesanan(nomorMejaLihat);
                    break;
                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi kasir restoran!");
                    break;
                default:
                    System.out.println("Pilihan tidak valid");
                    break;
            }
            System.out.println();
        }
        scanner.close();
    }
}